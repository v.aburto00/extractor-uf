package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class ExtractorUfApplication {

	public static void main(String[] args) {

		SpringApplication.run(ExtractorUfApplication.class, args);
		Scrapper Scrapper = new Scrapper();
		List<UF> UFS = Scrapper.extractUFS();
	}

}
