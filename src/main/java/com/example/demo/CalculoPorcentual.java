package com.example.demo;

public class CalculoPorcentual {
    public static float CalcularPorcentaje(float valorInicial, float valorObjetivo){
        float valorFinal = valorObjetivo - valorInicial;
        valorFinal = (valorFinal / valorInicial) * 100;
        return valorFinal;
    }
}
