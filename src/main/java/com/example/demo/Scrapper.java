package com.example.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Scrapper {

    private static final String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";

    public List<UF> extractUFS() {
        List<UF> UFS = new ArrayList<>();
        Document doc;
        try {
            doc = Jsoup.connect(URL).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Elements table = doc.select("tr").not(".GridHeader");
        for (Element row : table.subList( 1, table.size() )){
            String day = row.select("td").first().text();
            int mes = 0;
            Elements precios = row.getElementsByClass("obs");
            for(Element precio : precios){
                UF uf = new UF();
                mes++;
                if(!precio.text().isEmpty()) {
                    uf.setPrice(precio.text());
                    System.out.println(precio.text());
                    uf.setDate(day + "/"+mes+"/2021");
                    System.out.println(day + "/"+mes+"/2021");
                    UFS.add(uf);
                }
            }
        }
        return UFS;
    }
}
